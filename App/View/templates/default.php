<!DOCTYPE html>
<html>
<head lang="fr">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta charset="UTF-8">
    <title>Page defaut</title>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600' rel='stylesheet' type='text/css' />
</head>
<body>
    <?= $content; ?>
</body>
</html>