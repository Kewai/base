<?php

class Writing {
	
	private $data = [];

	public function __construct($data) {

		$this->data = $data;
	}

	public function build($file) {

		$path = CORE . DS . 'saved' . DS . $file . '.php';

		if(file_exists($path)) {
			
			return true;

		} else {
	
			$output = "<?php \n";
			$output .= "// File auto-generated.\n";	
			$output .= "\$savedForums = array(\n";
	
			foreach ($this->data as $key => $value) {
					
				$output .= "\t'".$key."' \t=> \t";
				if(is_array($value)) {
					$output .= "array(\n";
					foreach ($value as $k => $v) {
						$output .= "\t\t'" . $k . "'\t =>\t '" . addslashes($v) . "',\n";
					}
					$output .= "\t\t),\n";
				} else {
		
					$output .= "'".$value."',\n";
				}
			}
	
			$output .= ");";
	
			return file_put_contents($path, $output);
		}
	}

	public function check($data, $key, $value, $ask) {

		$j = count($data);
            $i = 0;
            $response = false;
            do {
                if($data[$i][$key] == $value) {
                    
                    $response = true;

                    	foreach ($ask as $k) {

                    		$list[$k] = $data[$i][$k];
                    			
                    	}
                }
                $i++;
            
            } while($i < $j AND $response == false);

            return $list;
	}
}