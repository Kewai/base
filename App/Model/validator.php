<?php
namespace App\Model;

use \Core\Session;

class Validator {

	private $sent = [];

    /**
     * @param $sent
     */
    public function __construct($sent) {
		
		$this->sent = $sent;
	}

    /**
     * @param $name
     * @param $rule
     * @param bool $options
     * @return bool
     */
    public function check($name, $rule, $options = false) {
		
		$validator = "validate_$rule";
		if(!$this->$validator($name, $options)) {
            Session::setError('Le champs ' . $name . ' n\'a pas été rempli correctement.', $name);
        }
        return true;
	}

    /**
     * @param $name
     * @return bool
     */
    public function validate_required($name) {

		return array_key_exists($name, $this->sent) && $this->sent[$name] != '';
	}

    /**
     * @param $name
     * @return bool
     */
    public function validate_email($name) {
		
		return array_key_exists($name, $this->sent) && filter_var($this->sent[$name], FILTER_VALIDATE_EMAIL);
	}

    /**
     * @param $name
     * @param $values
     * @return bool
     */
    public function validate_in($name, $values) {

		return array_key_exists($name, $this->sent) && in_array($this->sent[$name], $values);
	}

    /**
     * @param $password
     * @param $password2
     * @return bool
     */
    public function compare($password, $password2) {
		if($this->sent[$password] != $this->sent[$password2]) {
            return Session::setError('Vos mots de passe sont différents', $password);
		}
        return true;
	}

    /**
     * @param $name
     * @param $field
     * @return bool
     */
    public function length($name, $field) {

        $min = constant(strtoupper($name)."_MIN_LENGTH");
        $max = constant(strtoupper($name)."_MAX_LENGTH");
        $string = strlen($this->sent[$name]);

        if($string < $min OR $string > $max) {
            return Session::setError('Le champs ' . $field . ' est trop court il doit comporter entre '.$min.' et ' . $max . ' caractères', 'length_'.$name);
        }
        return true;
    }

    /**
     * @param $name
     * @return bool
     */
    public function special($name) {
        $string = htmlspecialchars($this->sent[$name]);

        if(preg_match('([^a-zA-Z0-9àáâãäåòóôõöøèéêëçìíîïùúûüÿñ*._-])', $string)) {
            return Session::setError('Seuls les caractères alpha-numérique et - . * _ sont acceptés', 'special_'.$name);
        }
        return true;
    }

    /**
     * @param string $last_post
     * @return bool
     */
    public function antiFlood($last_post = '') {

        $diff = time() - $last_post;
        if( $diff <= ANTI_FLOOD) {
            return Session::setError('Vous devez attendre une minute entre 2 messages', 'Flood limit');
        }
        return true;
    }
}
