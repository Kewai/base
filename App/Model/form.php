<?php

namespace App\Model;

class form {

    public $required = NULL;
    public $data = [];

    public function __construct($data = []) {
        $this->prefix = isset($data[0]) ? $data[0] : FORM_INPUT;
        $this->class = isset($data[1]) ? $data[1] : FORM_CLASS;
    }

    /**
     * @param $type
     * @param $name
     * @param $label
     * @param $required
     * @return string
     */
    private function input($type, $name, $label, $required) {

        $save = isset($_SESSION[$this->prefix][$name]) ? $_SESSION[$this->prefix][$name] : '';

        if($type == 'textarea') {

            $input = "<label for=\"input_$name\" class=\"field-label\">$label</label><textarea name=\"$name\" id=\"input_$name\" class=\"textarea-input\" rows=\"7\" $required>$save</textarea>";
            return $this->buildTextarea($input);

        }elseif ($type == 'checkbox' OR $type == 'radio') {

            $input = "<label for=\"input_$name\" class=\"checkbox-label\"><input type=\"$type\" id=\"input_$name\" name=\"$name\" /><span class=\"checkbox\"></span>$label</label>";
            return $this->buildBox($input);

        } else {

            $input = "<label for=\"input_$name\" class=\"field-label\">$label</label><input type=\"$type\" id=\"input_$name\" name=\"$name\" class=\"field-input\"  value=\"$save\" $required />";
            return $this->buildField($input);
        }

    }

    public function text($name, $label, $required = '') {

        return $this->input('text', $name, $label, $required);
    }

    public function password($name, $label, $required = '') {

        return $this->input('password', $name, $label, $required);
    }

    public function email($name, $label, $required = '') {

        return $this->input('email', $name, $label, $required);
    }


    public function buildField($input) {
        return '<div class="field">
                                '.$input.'
                            </div>';
    }

    public function textarea($name, $label, $required = '') {

        return $this->input('textarea', $name, $label, $required);
    }

    public function buildTextarea($input) {
        return '<div class="textarea">
                                '.$input.'
                            </div>';
    }

    public function hidden($name, $data) {

        $input = "<input type=\"hidden\" name=\"$name\" value=\"$data\" required />";
        return $input;
    }

    public function checkbox($name, $label, $required = '') {

        return $this->input('checkbox', $name, $label, $required);
    }

    public function buildBox($input) {
        return '<div class="checkbox-choice">
                                '.$input.'
                            </div>';
    }

}