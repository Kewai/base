<?php

namespace App\Controller;

use Core\Controller\Controller;

class OriginController extends Controller {

      protected $template = 'default';

      public function __construct($value='') {
      	$this->viewPath = ROOT . DS . 'App/View/';
      }
}
