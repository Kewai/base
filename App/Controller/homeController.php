<?php

namespace App\Controller;

class HomeController extends OriginController {

      public function index() {

          $vars = compact('');
          $this->render('home', $vars);

      }
}
