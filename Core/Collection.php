<?php

namespace Core;

class Collection implements \IteratorAggregate, \ArrayAccess{


    //private $items;

    /**
     * @param array $items
     */
    public function __construct(array $items) {

        $this->items = $items;

    }

    /**
     * Give the value of key from Collection
     * @param $key
     * @return string
     */
    public function get($key) {

        $index = explode('.', $key);
        return $this->getValue($index, $this->items);
    }

    /**
     * Private function using by get method
     *
     * @param array $indexes
     * @param $value
     * @return Collection|null
     */
    private function getValue(array $indexes, $value) {

        $key = array_shift($indexes);
        if(empty($indexes)) {
            if(!array_key_exists($key, $value)) {
                return null;
            }
            if(is_array($value[$key])) {
                return new Collection($value[$key]);
            }
            return $value[$key];
        }
        return $this->getValue($indexes, $value[$key]);
    }

    /**
     * Add an object after construct
     * @param $key
     * @param $value
     */
    public function set($key, $value) {

        $this->items[$key] = $value;
    }

    /**
     * Check if $key exist
     * @param $key
     * @return bool
     */
    public function has($key) {

        return array_key_exists($key, $this->items);
    }

    /**
     * @param $key
     * @return Collection
     */
    public function extract($key) {

        $results = [];
        foreach ($this->items as $item) {
            $results[] = $item[$key];
        }
        return new Collection($results);
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Whether a offset exists
     * @link http://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     */
    public function offsetExists($offset) {

        return $this->has($offset);
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to retrieve
     * @link http://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed Can return all value types.
     */
    public function offsetGet($offset) {

        return $this->get($offset);
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to set
     * @link http://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     */
    public function offsetSet($offset, $value) {

        $this->set($offset, $value);
        return null;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to unset
     * @link http://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     */
    public function offsetUnset($offset) {

        if($this->has($offset)) {
            unset($this->items[$offset]);
        }
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Retrieve an external iterator
     * @link http://php.net/manual/en/iteratoraggregate.getiterator.php
     * @return Traversable An instance of an object implementing <b>Iterator</b> or
     * <b>Traversable</b>
     */
    public function getIterator() {

        return new ArrayIterator($this->items);
    }
}