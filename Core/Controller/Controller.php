<?php
namespace Core\Controller;

use \Core\Auth;
use \Core\Database;
use \App\Model\RainTPL;
use \Core\Session;
use \Core\AltoRouter;

class Controller {

    public $session;
    public $auth;
    protected $db;
    protected $viewPath;
    protected $template;

    public function __construct() {

        // Instances
        $this->db = new Database('localhost', 'root', 'root', 'community');
        $this->session = new Session();
        $this->auth = new Auth();

        // Notifications
        if(isset($_SESSION['flash'])) {

            $flash = Session::show('flash');

            $array = array(
                'TYPE' => key($flash),
                'MESSAGE' => current($flash)
            );

            $this->tpl->assign('flash', $array);
        }
    }

    public function Title($name) {
        $title = array('TITLE' => $name);
    }

    public function CSS(array $sheet) {

        $path = [];
        foreach($sheet as $name) {
            $path[] = BASE_URL . DS . $name . '.css';
        }
        $this->tpl->assign('CSS', $path);
    }

    public function JS(array $sheet) {

        $path = [];
        foreach($sheet as $name) {
            $path[] = BASE_URL . DS . $name . '.js';
        }
        $this->tpl->assign('JS', $path);
    }

    public function Level($min) {

        if ( $this->auth->level <= $min ) {
            $url = AltoRouter::generate('admin', []);
            header('Location:'.$url);
        }
    }

    /**
     * @param $count : nombre d'enregistrement dans la table
     * @param $limit : affichage dans une page
     * @param $url : nom de la page pour la pagination
     * @param string $params : pour l'url
     * @return mixed
     * @throws Exception
     */
    protected function Pagination($count, $limit, $url, $params = '') {

        $countPage = ceil($count / $limit);

        for ($i=1; $i <= $countPage; $i++) {
            $pagination[] = [
                'PAGINATION'        =>  $i,
                'GOTO'              => PAGINATION_ALT . $i,
                'PAGINATION_URL'    =>  AltoRouter::generate($url, array_merge($params, ['page' => $i]))];
        }

        $this->tpl->assign('pagination', $pagination);
    }

    /**
     * Créé la limit du select pour la requête
     * @param string $page
     * @return array
     */
    protected function whichPageIShowYou($page = '') {

        $page = !empty($page) ? $page : '1';
        $min = ($page * LIMIT_TOPICS) - (LIMIT_TOPICS);
        $limit = array($min, LIMIT_TOPICS);

        return $limit;
    }

    protected function checkAndShow() {

        // Check if some errors exist and send to the template
        if(isset($_SESSION['errors'])) {

            $errors = implode('<p>', Session::show('errors'));
            $this->tpl->assign('errors', $errors);
        }
        return null;
    }

    //
    // Loaders
    //

    /**
     * @param $model : nom du Model
     * @param string $options
     * @return mixed
     */
    public function Model($model, $options = '') {

        return new $model($options);
    }

    /**
     * @param $table
     * @return mixed
     */
    public function Table($table) {

        $className = '\\App\\Model\\Table\\' . $table . 'Table';
        require_once ROOT . DS . 'App/Entity/' . $table . 'Entity.php';

        return new $className();
    }

    /**
     * @param $file
     * @return bool
     */
    public function Saved($file) {

        $path = CORE . DS . 'saved' . DS . $file . '.php';
        if(file_exists($path)) {

            return require_once $path;
        } else {
            return null;
        }
    }

    public function render($view, $vars = []) {
        
        ob_start();
        extract($vars);

        require($this->viewPath . str_replace('.', '/', $view) . '.php');

        $content = ob_get_clean();
        require($this->viewPath . 'templates/' . $this->template . '.php');
    }
}
