<?php

namespace Core;

class Session {

    /*
     * Starting session() everywhere !
     */
    public function __construct() {

        session_start();
    }

    // Create a key session
    public static function set($type, $name, $data) {

        return $_SESSION[$type][$name] = $data;
    }

    public static function setFlash($message, $name) {

        return self::set('flash', $name, $message);
    }

    public static function setError($message, $name) {

        return self::set('errors', $name, $message);
    }

    public static function show($key) {

        $error =  $_SESSION[$key];
        unset($_SESSION[$key]);
        return $error;
    }
} 