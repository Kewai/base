<?php

namespace Core;

use \App\Model\Users;

class Auth {

    public $logged = false;

    public function __construct() {

        if(isset($_SESSION['logged']['name'])) {

            $this->arrayToObject($_SESSION['logged']);
        } elseif(isset($_COOKIE['auth'])) {

            $this->takeMyCookie();
        } else {

            $this->lastTopic =  isset($_SESSION['logged']['lastTopic']) ? $_SESSION['logged']['lastTopic'] : '0';
        }
    }

    public function login($obj, $form, $fromCookie = false) {
        if(is_null($obj)) {
            return null;
        }

        if($fromCookie == true) {
            $key = $form[1];
            $toCompare = hash_password($obj->email, $obj->password);
        } else {
            $key = hash_password($form['name'], $form['password']);
            $toCompare = $obj->password;
        }

        if ($key == $toCompare) {

            $array = array(
                'id'            =>      $obj->id,
                'name'          =>      $obj->name,
                'type'          =>      $obj->type,
                'level'         =>      $obj->level,
                'timezone'      =>      $obj->timezone,
                'antiFlood'     =>      0,
                'lastTopic'     =>      0
            );


            // CHECKBOX REMEMBER
            if (isset($form['remember'])){
                $this->bakeMeACookie($obj->id, $obj->email, $obj->password);
            }

            $this->arrayToSession($array);

            if($fromCookie == true) {
                $this->bakeMeACookie($obj->id, $obj->email, $obj->password);
                return $this->arrayToObject($array);
            } else {

                return Session::setFlash('Vous êtes maintenant identifié !', 'success');
            }

        } else {
            if($fromCookie == true) {
                return $this->trashMyCookie();
            } else {

                return Session::setError('Mot de passe invalide.', 'identification');
            }
        }
    }

    private function arrayToObject(array $items) {

        foreach ($items as $key => $value) {
            $this->$key = $value;
        }
        $this->logged = true;
    }

    private function arrayToSession(array $array) {

        foreach ($array as $key => $value) {

            Session::set('logged', $key, $value);
        }
    }

    private function bakeMeACookie($id, $email, $password) {
        return setcookie('auth', $id . '9hj06w8q62z98mf03p79' . hash_password($email, $password) , time() + 3600 * 24 * 30, '/', DOMAIN, false, true);

    }

    private function takeMyCookie() {

        $cookie =  explode('9hj06w8q62z98mf03p79', $_COOKIE['auth']);
        $db = Database::getInstance();

        $user = $db->where('id', $cookie[0])
                    ->backState('auth')
                    ->getOne('users');

        $logged = $this->login($user, $cookie, true);

    }

    private function trashMyCookie() {

        return setcookie('auth', null, time() - 3600 * 24 * 30, '/', DOMAIN, false, true);
    }

    /**
     *
     */
    public function alreadyLogged() {

        if($this->logged == true) {

            Session::setFlash('Vous êtes déjà connecté', 'info');
            Header('Location:/community/info');
            exit;
        }
    }

    public function needLogged() {

        if($this->logged == false) {

            Session::setFlash('Vous devez être identifié', 'warning');
            Header('Location:/community/login');
            exit;
        }
    }

    public function antiFlood() {

        return Session::set('logged', 'antiFlood', time());
    }

    /**
     * Bye-bye ! :'(
     */
    public function logOut() {

        session_destroy();
        $this->trashMyCookie();

    }
} 