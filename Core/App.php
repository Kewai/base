<?php

use Core\Error;

class App {

	protected $request = [];
    protected $controller;
    protected $method;
    protected $fileController;
	protected $params = [];
    private static $_instance;

    public function __construct() {

        static::loading();

    }

    public static function loading() {

        require 'AutoLoader.php';
        Core\AutoLoader::register();
        require CORE . DS . 'AutoLoader.php';
        App\AutoLoader::register();
        require ROOT . DS . 'Libs/AutoLoader.php';
        Libs\AutoLoader::register();
    }

    public static function getInstance() {
        if(is_null(static::$_instance)) {
            static::$_instance = new App();
        }
        return static::$_instance;
    }

    /**
     * Call the target Controller
     * @param $match
     */
    public function build($match) {

		// Isolation of Controller#Method
		$request = explode('#', $match['target']);

		// Set Controller, Method default and params
		$this->controller = $request[0] . 'Controller';
		$this->method = isset($request[1]) ? $request[1] : 'index';
		$this->params = $match['params'];

		// Include the Controller
        $file = CORE . DS . 'Controller/' . $this->controller . '.php';
		if(file_exists($file)) {
            $this->fileController = '\\App\\Controller\\' . str_replace('/', '\\', $this->controller);
        }

		// Call the class -> method  with params
		if(method_exists($this->fileController, $this->method)) {
            call_user_func_array([new $this->fileController, $this->method], $this->params);
		} else {
            $error = new Error();
			$message = 'La méthode ' . $this->method . ' pour le controller ' . $this->controller . ' est introuvable';
            $error->e404($message);
		}
	}
}
