<?php
namespace Core;

use Core\Controller\Controller;

class Error extends Controller {

    public function e404($message) {

        header("HTTP/1.0 404 Not Found");

        echo $message;
    }

    public static function checkError($page) {

        if(!empty($_SESSION['errors'])) {
            Header('Location:'.$page);
            die();
        }
    }

    public static function checkErrorForAjax($page) {

        if(!empty($_SESSION['errors'])) {
            header('500 internal server error', true, 500);
            die("Le champs n'a pas été rempli");
        }
    }

    static function instance($message) {

        return static::e404($message);
    }

    public static function redirect($url) {

        header("HTTP/1.1 301 Moved Permanently");
        header('Location :'.$url);
    }

}