<?php

/**
 * Return a sha1 password
 * @param $pseudo
 * @param $password
 * @return string
 */
function hash_password($pseudo, $password) {
    $password = sha1(strtolower($pseudo).$password."UshouldnotdoThat");
    return $password;
}

function slug($string) {
    $data = trim($string);
    $data = preg_replace('/[^a-zA-Z0-9\/_|+ -]/', '', $data);
    $data = strtolower(trim($data, '-'));
    $data = preg_replace('/[\/_|+ -]+/', '-', $data);
    return $data;
}

// Suppression des antislashes d'une chaine de caractère
function htmlConvert($string) {
    $string = htmlspecialchars($string);
    return $string;
}
// Suppression des antislashes d'une chaine de caractère
function htmlUnconvert($string) {
    $string = htmlspecialchars_decode($string);
    return $string;
}

// Transformation d'un timestamp de la bdd en date affichable
function timestampToDate($string) {
    $string = (date('H:i:s \l\e d/m/Y',$string));
    return $string;
}

// Transformation d'un timestamp de la bdd en date affichable
function timestampToDateSimplified($string) {
    $string = (date('H\hi \l\e d/F/Y',$string));
    return $string;
}

function ariane($array) {
    $fil = '';
        foreach($array as $key => $value) {
            if($key == 'final') {
                $fil .= $value;
                break;
            }
            $fil .= '<a href="' . $value . '" alt="">' . $key . '</a> > ';
        }
        echo $fil;
    
}