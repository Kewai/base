<?php

$start = microtime(true);

// constant path
define('WEBROOT', dirname(__FILE__));
define('ROOT', dirname(WEBROOT));
define('DS', DIRECTORY_SEPARATOR);
define('CORE', ROOT.DS.'App');
define('BASE_URL', dirname(dirname($_SERVER['SCRIPT_NAME'])));

// Include files
require ROOT . DS . 'config/settings.php';
require ROOT . DS . 'config/functions.php';
require ROOT . DS . 'core/App.php';


// Instances

$app = new App();
$router = new Core\AltoRouter();

// Set BasePath
$router->setBasePath(BASE_URL);

// map homepage
$router->map( 'GET', '', 'home', 'home');

// Match the route
$match = $router->match();

if ($match) {
	$app->build($match);
} else {
    $error = new \Core\Error();
    $message = 'La page est introuvable.';
    $error->e404($message);

}

$end = microtime(true);

$total = $end - $start;
//var_dump('the time => '.$total);